package errors

import "fmt"

var (
	TokenTypeError                = fmt.Errorf("token type unknown")
	TokenExtractUserError         = fmt.Errorf("type assertion to user err")
	AuthServiceInvalidCredentials = fmt.Errorf("auth service: invalid credentials")
)
