package controller

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/warming-spb/goboilerplate/internal/infrastructure/component"
	"gitlab.com/warming-spb/goboilerplate/internal/infrastructure/errors"
	"gitlab.com/warming-spb/goboilerplate/internal/infrastructure/handlers"
	"gitlab.com/warming-spb/goboilerplate/internal/infrastructure/responder"
	"gitlab.com/warming-spb/goboilerplate/internal/modules/user/service"
	"net/http"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	GetUsersInfo(http.ResponseWriter, *http.Request)
	ChangePassword(w http.ResponseWriter, r *http.Request)
}

type User struct {
	service service.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service service.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	out := u.service.GetByID(r.Context(), service.GetByIDIn{UserID: claims.ID})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	u.OutputJSON(w, ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: Data{
			User: *out.User,
		},
	})
}

func (u *User) GetUsersInfo(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}

func (u *User) ChangePassword(w http.ResponseWriter, r *http.Request) {
	var req ChangePasswordRequest
	err := u.Decode(r.Body, &req)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}

	if req.NewPassword != req.ConfirmPassword {
		u.OutputJSON(w, ChangePasswordResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: ChangePasswordData{
				Message: "New password and confirmation password do not match",
			},
		})
		return
	}

	// Извлечение идентификатора пользователя из токена
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}

	// Вызов сервиса аутентификации для смены пароля
	out := u.service.ChangePassword(r.Context(), service.ChangePasswordIn{
		UserID:      claims.ID,
		OldPassword: req.OldPassword,
		NewPassword: req.NewPassword,
	})

	if out.ErrorCode == errors.AuthServiceWrongPasswordErr {
		u.OutputJSON(w, ChangePasswordResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: ChangePasswordData{
				Message: "Invalid old password",
			},
		})
		return
	}

	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, ChangePasswordResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: ChangePasswordData{
				Message: "Failed to change password",
			},
		})
		return
	}

	u.OutputJSON(w, ChangePasswordResponse{
		Success: true,
		Data: ChangePasswordData{
			Message: "Password changed successfully",
		},
	})
}
