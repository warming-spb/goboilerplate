package modules

import (
	"gitlab.com/warming-spb/goboilerplate/internal/infrastructure/component"
	aservice "gitlab.com/warming-spb/goboilerplate/internal/modules/auth/service"
	uservice "gitlab.com/warming-spb/goboilerplate/internal/modules/user/service"
	"gitlab.com/warming-spb/goboilerplate/internal/storages"
)

type Services struct {
	User uservice.Userer
	Auth aservice.Auther
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
